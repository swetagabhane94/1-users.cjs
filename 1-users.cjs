const users = {
  John: {
    age: 24,
    desgination: "Senior Golang Developer",
    interests: ["Chess, Reading Comics, Playing Video Games"],
    qualification: "Masters",
    nationality: "Greenland",
  },
  Ron: {
    age: 19,
    desgination: "Intern - Golang",
    interests: ["Video Games"],
    qualification: "Bachelor",
    nationality: "UK",
  },
  Wanda: {
    age: 24,
    desgination: "Intern - Javascript",
    interests: ["Piano"],
    qualification: "Bachaelor",
    nationality: "Germany",
  },
  Rob: {
    age: 34,
    desgination: "Senior Javascript Developer",
    interest: ["Walking his dog, Cooking"],
    qualification: "Masters",
    nationality: "USA",
  },
  Pike: {
    age: 23,
    desgination: "Python Developer",
    interests: ["Listing Songs, Watching Movies"],
    qualification: "Bachaelor's Degree",
    nationality: "Germany",
  },
};

// Q1 Find all users who are interested in playing video games.




// Q2 Find all users staying in German
const countryName = Object.entries(users).filter((data) => {
  return data[1].nationality === "Germany";
});

//console.log(countryName);

// Q3 Sort users based on their seniority level
//    for Designation - Senior Developer > Developer > Intern
//    for Age - 20 > 10
// Q4 Find all users with masters Degree.

const degreeDetail = Object.entries(users).filter((data) => {
  return data[1].qualification === "Masters";
});

//console.log(degreeDetail)

// Q5 Group users based on their Programming language mentioned in their designation.
const programDesignation = Object.entries(users).reduce((data,value) => {
    return {...data,[value[1].desgination]: (data[value[1].desgination] ||0) +1}
});


